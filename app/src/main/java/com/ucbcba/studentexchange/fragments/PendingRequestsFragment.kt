package com.ucbcba.studentexchange.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ucbcba.studentexchange.R
import com.ucbcba.studentexchange.adapters.AdapterPendingListener
import com.ucbcba.studentexchange.adapters.PendingRequestListAdapter
import com.ucbcba.studentexchange.entities.PendingRequest
import com.ucbcba.studentexchange.entities.Request
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_pending_requests.*

/**
 * A simple [Fragment] subclass.
 */
class PendingRequestsFragment (val adapterPendingListener: AdapterPendingListener): Fragment() {

    lateinit var adapterPending:AdapterPendingListener

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        adapterPending = adapterPendingListener
        return inflater.inflate(R.layout.fragment_pending_requests, container, false)
    }

    override fun onStart() {
        super.onStart()
        val db : FirebaseFirestore = FirebaseFirestore.getInstance()
        val requests: MutableList<PendingRequest> = ArrayList()
        val docRef = db.collection("requests")
        docRef.whereEqualTo("receiver", FirebaseAuth.getInstance().currentUser!!.email.toString()).get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    for (document in task.result!!) {
                        if (document.exists()) {
                            val request:Request = document.toObject(Request::class.java)
                            requests.add(PendingRequest(request.receiver.toString(),request.sender.toString(),request.status.toString()))
                        }
                    }
                    pending_request_list_recycler_view.apply{
                        layoutManager = LinearLayoutManager(activity)
                        adapter = PendingRequestListAdapter(
                            requests,
                            this.context,adapterPending
                        )
                    }
                } else {
                    Toast.makeText(
                        context,
                        task.exception.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
}
