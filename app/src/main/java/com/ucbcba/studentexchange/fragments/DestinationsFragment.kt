package com.ucbcba.studentexchange.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.ucbcba.studentexchange.entities.Destination
import com.ucbcba.studentexchange.adapters.DestinationsListAdapter
import com.ucbcba.studentexchange.R
import com.ucbcba.studentexchange.adapters.AdapterListener
import com.ucbcba.studentexchange.entities.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_destinations.*

/**
 * A simple [Fragment] subclass.
 */
class DestinationsFragment(val adapterListener: AdapterListener) : Fragment() {

    lateinit var adapterL: AdapterListener

    override fun onCreateView(

        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        adapterL = adapterListener


        return inflater.inflate(R.layout.fragment_destinations, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val db : FirebaseFirestore = FirebaseFirestore.getInstance()
        val users: MutableList<Destination> = ArrayList()
        db.collection("users")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    val user:User = document.toObject(User::class.java)
                    if(user.email.toString() != FirebaseAuth.getInstance().currentUser?.email)
                    {
                        users.add(Destination(user.email.toString(),user.university.toString()))
                    }
                }
                destinations_list_recycler_view.apply{
                    layoutManager = LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,true)
                    adapter =
                        DestinationsListAdapter(
                            users,
                            this.context,adapterL
                        )
                }
            }
            .addOnFailureListener {
                Toast.makeText(context, "no encontrado", Toast.LENGTH_LONG).show()
            }

    }


}

