package com.ucbcba.studentexchange.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ucbcba.studentexchange.R
import com.ucbcba.studentexchange.entities.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_profile.*

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onStart() {
        super.onStart()
        val db: FirebaseFirestore = FirebaseFirestore.getInstance()
        val emailUser = FirebaseAuth.getInstance().currentUser!!.email.toString()
        val docRef = db.collection("users").document(emailUser)
        docRef.get().addOnSuccessListener { documentSnapshot ->
            val user = documentSnapshot.toObject(User::class.java)
            full_name_id.text = user!!.firstName.toString() + " " + user.lastName.toString()
            email_id.text = user.email.toString()
            university_id.text = user.university.toString()
            country_id.text = user.country.toString()
            city_id.text = user.city.toString()
        }
    }
}
