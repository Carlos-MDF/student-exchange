package com.ucbcba.studentexchange.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ucbcba.studentexchange.R
import com.ucbcba.studentexchange.adapters.AdapterRequestListener
import com.ucbcba.studentexchange.adapters.MyRequestListAdapter
import com.ucbcba.studentexchange.entities.MyRequest
import com.ucbcba.studentexchange.entities.Request
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_my_requests.*

/**
 * A simple [Fragment] subclass.
 */
class MyRequestsFragment(val adapterRequestListener: AdapterRequestListener) : Fragment() {

    lateinit var adapterListener:AdapterRequestListener

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        adapterListener = adapterRequestListener
        return inflater.inflate(R.layout.fragment_my_requests, container, false)
    }

    override fun onStart() {
        super.onStart()
        val db : FirebaseFirestore = FirebaseFirestore.getInstance()
        val requests: MutableList<MyRequest> = ArrayList()
        val docRef = db.collection("requests")
        docRef.whereEqualTo("sender", FirebaseAuth.getInstance().currentUser!!.email.toString()).get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    for (document in task.result!!) {
                        if (document.exists()) {
                            val request:Request = document.toObject(Request::class.java)
                            requests.add(MyRequest(request.receiver.toString(),request.sender.toString(),request.status.toString()))
                        }
                    }
                    my_request_list_recycler_view.apply{
                        layoutManager = LinearLayoutManager(activity)
                        adapter = MyRequestListAdapter(
                            requests,
                            this.context,adapterListener
                        )
                    }
                } else {
                    Toast.makeText(
                        context,
                        task.exception.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

}
