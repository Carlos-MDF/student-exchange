package com.ucbcba.studentexchange.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ucbcba.studentexchange.entities.Destination
import com.ucbcba.studentexchange.R
import kotlinx.android.synthetic.main.destinations_row.view.*

class DestinationsListAdapter(
    val list: MutableList<Destination>,
    val context: Context?,
    val adapterListener: AdapterListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var adapter: AdapterListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        adapter = adapterListener
        val inflater = LayoutInflater.from(parent.context)
        return DestinationsListViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val destinations = list.get(position)
        holder.itemView.destiny_text_view.text = destinations.destiny
        holder.itemView.destination_user_text_view.text = destinations.destination_user
        holder.itemView.image_button_send_request.setOnClickListener {
            adapter.onSendClick(destinations.destiny)
        }
    }
}

interface AdapterListener {
    fun onSendClick(destino: String)
}

class DestinationsListViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.destinations_row, parent, false)) {
}