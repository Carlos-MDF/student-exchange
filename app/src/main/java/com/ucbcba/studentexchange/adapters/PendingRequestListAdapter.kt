package com.ucbcba.studentexchange.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ucbcba.studentexchange.R
import com.ucbcba.studentexchange.entities.PendingRequest
import kotlinx.android.synthetic.main.pending_request_row.view.*

class PendingRequestListAdapter(val list: MutableList<PendingRequest>, val context: Context?, val adapterPendingListener: AdapterPendingListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    lateinit var adapterPending: AdapterPendingListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        adapterPending = adapterPendingListener
        val v = LayoutInflater.from(parent.context).inflate(R.layout.pending_request_row,parent,false)
        return PendingRequestListViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val pendingRequest = list.get(position)
        holder.itemView.destiny_pending_text_view.text = pendingRequest.destiny
        holder.itemView.user_pending_text_view.text = pendingRequest.user
        holder.itemView.image_button_accept_pending_request.setOnClickListener{
            adapterPending.onAcceptClick(pendingRequest.destiny)
        }
        holder.itemView.image_button_reject_pending_request.setOnClickListener{
            adapterPending.onRejectClick(pendingRequest.destiny)
        }
    }

}

interface AdapterPendingListener {
    fun onAcceptClick(destino: String)
    fun onRejectClick(destino: String)
}

class PendingRequestListViewHolder(view: View) : RecyclerView.ViewHolder(view)