package com.ucbcba.studentexchange.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ucbcba.studentexchange.entities.MyRequest
import com.ucbcba.studentexchange.R
import kotlinx.android.synthetic.main.my_request_row.view.*

class MyRequestListAdapter(val list: MutableList<MyRequest>, val context: Context?, val adapterRequestListener: AdapterRequestListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var adapterListener: AdapterRequestListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        adapterListener = adapterRequestListener
        val v = LayoutInflater.from(parent.context).inflate(R.layout.my_request_row,parent,false)
        return MyRequestListViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val myRequest = list.get(position)
        holder.itemView.destiny_text_view.text = myRequest.destiny
        holder.itemView.status_text_view.text = myRequest.status
        holder.itemView.user_text_view.text = myRequest.user
        holder.itemView.image_button_reject_request.setOnClickListener{
            adapterListener.onDeleteRequest(myRequest.status)
        }
    }

}

interface AdapterRequestListener {
    fun onDeleteRequest(destino: String)
}

class MyRequestListViewHolder(view: View) : RecyclerView.ViewHolder(view)
