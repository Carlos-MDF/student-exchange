package com.ucbcba.studentexchange.presenters

import android.content.Context
import android.os.Handler
import android.widget.Toast
import com.ucbcba.studentexchange.R
import com.ucbcba.studentexchange.interfaces.IContractLogin
import com.google.firebase.auth.FirebaseAuth

class LoginPresenter(val view: IContractLogin.View, val context: Context) : IContractLogin.Presenter {
    override fun login(email: String, password: String) {
        view.showLoading()
        val r = Runnable {
            view.hideLoading()
            if(email.isEmpty()) {
                view.showErrorEmail(context.getString(R.string.user_edit_text_error_empty))
            }
            else{
                val mAuth: FirebaseAuth?
                mAuth = FirebaseAuth.getInstance()
                mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(context, "Login successful!", Toast.LENGTH_LONG).show()
                            view.loginSuccess()
                        } else {
                            view.loginFailed("Login failed! Please try again later")
                        }
                    }
            }
        }
        Handler().postDelayed(r, 3000)
    }
}