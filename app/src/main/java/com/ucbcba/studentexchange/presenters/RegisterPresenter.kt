package com.ucbcba.studentexchange.presenters

import android.content.Context
import android.os.Handler
import android.widget.Toast
import com.ucbcba.studentexchange.R
import com.ucbcba.studentexchange.interfaces.IContractRegister
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class RegisterPresenter(val view: IContractRegister.View, val context: Context) : IContractRegister.Presenter {
    override fun register(university:String, firstname:String, lastname:String, email:String, password:String, cpassword:String,country:String, city:String) {
        view.showLoading()
        val r = Runnable {
            view.hideLoading()
            view.cleanErrorMessages()
            val mAuth: FirebaseAuth?
            mAuth = FirebaseAuth.getInstance()
            val db = FirebaseFirestore.getInstance()
            when {
                firstname.isEmpty() -> view.showErrorFirstname(context.getString(R.string.error_message_first_name))
                lastname.isEmpty() -> view.showErrorLastname(context.getString(R.string.error_message_last_name))
                university.isEmpty() -> view.showErrorUniversity(context.getString(R.string.error_message_university))
                country.isEmpty() -> view.showErrorCountry(context.getString(R.string.error_message_country))
                city.isEmpty() -> view.showErrorCity(context.getString(R.string.error_message_city))
                email.isEmpty() -> view.showErrorEmail(context.getString(R.string.error_message_empty_email))
                password.isEmpty() -> view.showErrorPassword(context.getString(R.string.error_message_password))
                cpassword.isEmpty() -> view.showErrorConfirmPassword(context.getString(R.string.error_message_confirm_password))
                password != cpassword -> {
                    view.showErrorPassword(context.getString(R.string.error_message_password_not_equal))
                    view.showErrorConfirmPassword(context.getString(R.string.error_message_password_not_equal))
                }

                else -> mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            val user: MutableMap<String, Any> = HashMap()
                            user["firstName"] = firstname
                            user["lastName"] = lastname
                            user["university"] = university
                            user["country"] = country
                            user["city"] = city
                            user["email"] = email
                            db.collection("users").document(email)
                                .set(user)
                            Toast.makeText(context, "Registration successful!", Toast.LENGTH_LONG).show()
                            view.registerSuccess()
                        } else {
                            view.registerFailed("Registration failed! Please try again later")
                        }
                    }
            }
        }
        Handler().postDelayed(r, 6000)
    }

}