package com.ucbcba.studentexchange.interfaces

interface IContractLogin {
    interface View {
        fun showErrorEmail(message:String)
        fun showLoading()
        fun hideLoading()
        fun loginSuccess()
        fun loginFailed(message: String)
    }

    interface Presenter {
        fun login(email:String, password:String)
    }
}