package com.ucbcba.studentexchange.interfaces

interface IContractRegister {
    interface View {
        fun showErrorUniversity(message:String)
        fun showErrorCountry(message:String)
        fun showErrorCity(message:String)
        fun showErrorFirstname(message:String)
        fun showErrorLastname(message:String)
        fun showErrorEmail(message:String)
        fun showErrorPassword(message:String)
        fun showErrorConfirmPassword(message:String)
        fun registerFailed(message:String)
        fun cleanErrorMessages()
        fun showLoading()
        fun hideLoading()
        fun registerSuccess()
    }

    interface Presenter {
        fun register(university:String, firstname:String, lastname:String, email:String, password:String, cpassword:String,country:String,city:String)
    }

}