package com.ucbcba.studentexchange.activities

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ucbcba.studentexchange.R
import com.ucbcba.studentexchange.interfaces.IContractRegister
import com.ucbcba.studentexchange.presenters.RegisterPresenter
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.progressBar
import java.io.IOException
import java.util.*


class RegisterActivity : AppCompatActivity(), IContractRegister.View {

    private val PICK_IMAGE_REQUEST = 1234

    private var filePath: Uri? = null

    internal var storage:FirebaseStorage? = null
    internal var storageReference: StorageReference? = null

    private lateinit var registerPresenter: IContractRegister.Presenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        registerPresenter = RegisterPresenter(this,applicationContext)

        storage = FirebaseStorage.getInstance()
        storageReference = storage!!.reference

        uploadImg.setOnClickListener {
            showFileChooser()
        }

        buttonRegister.setOnClickListener {
            uploadFile()
            registerPresenter.register(textInputEditTextUniversity.text.toString(), textInputEditTextFirstName.text.toString(), textInputEditTextLastName.text.toString(), textInputEditTextEmail.text.toString(), textInputEditTextPassword.text.toString(), textInputEditTextConfirmPassword.text.toString(), textInputEditCountry.text.toString(), textInputEditCity.text.toString())
        }
    }

    override fun showErrorUniversity(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        textInputLayoutUniversity.error = message
        textInputEditTextUniversity.error = message
    }

    override fun showErrorCountry(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        textInputLayoutCountry.error = message
        textInputEditCountry.error = message
    }

    override fun showErrorCity(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        textInputLayoutCity.error = message
        textInputEditCity.error = message
    }

    override fun showErrorFirstname(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        textInputLayoutFirstName.error = message
        textInputEditTextFirstName.error = message
    }

    override fun showErrorLastname(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        textInputLayoutLastName.error = message
        textInputEditTextLastName.error = message
    }

    override fun showErrorEmail(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        textInputLayoutEmail.error = message
        textInputEditTextEmail.error = message
    }

    override fun showErrorPassword(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        textInputLayoutPassword.error = message
        textInputEditTextPassword.error = message
    }

    override fun showErrorConfirmPassword(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        textInputLayoutConfirmPassword.error = message
        textInputEditTextConfirmPassword.error = message
    }

    override fun cleanErrorMessages() {
        textInputLayoutUniversity.error = null
        textInputEditTextUniversity.error = null
        textInputLayoutFirstName.error = null
        textInputEditTextFirstName.error = null
        textInputLayoutLastName.error = null
        textInputEditTextLastName.error = null
        textInputLayoutEmail.error = null
        textInputEditTextEmail.error = null
        textInputLayoutPassword.error = null
        textInputEditTextPassword.error = null
        textInputLayoutConfirmPassword.error = null
        textInputEditTextConfirmPassword.error = null
        textInputLayoutCountry.error = null
        textInputEditCountry.error = null
        textInputLayoutCity.error = null
        textInputEditCity.error = null
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
        userImage.visibility = View.GONE
        university.visibility = View.GONE
        country.visibility = View.GONE
        city.visibility = View.GONE
        firstname.visibility = View.GONE
        lastname.visibility = View.GONE
        email.visibility = View.GONE
        password.visibility = View.GONE
        cpassword.visibility = View.GONE
        buttonRegister.visibility = View.GONE
        loginLink.visibility = View.GONE
        uploadImg.visibility = View.GONE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
        userImage.visibility = View.VISIBLE
        university.visibility = View.VISIBLE
        country.visibility = View.VISIBLE
        city.visibility = View.VISIBLE
        firstname.visibility = View.VISIBLE
        lastname.visibility = View.VISIBLE
        email.visibility = View.VISIBLE
        password.visibility = View.VISIBLE
        cpassword.visibility = View.VISIBLE
        buttonRegister.visibility = View.VISIBLE
        loginLink.visibility = View.VISIBLE
        uploadImg.visibility = View.VISIBLE
    }

    override fun registerSuccess() {
        val intent = Intent(this, WelcomeActivity::class.java)
        startActivity(intent)
    }

    override fun registerFailed(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            filePath = data.data
            try {
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
                userImage.setImageBitmap(bitmap)
            } catch (e:IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun uploadFile() {
        if(filePath != null) {
            val progressDialog = ProgressDialog(this)
            progressDialog.setTitle("Uploading...")
            progressDialog.show()

            val imageRef = storageReference!!.child("images/"+ UUID.randomUUID().toString())
            imageRef.putFile(filePath!!)
                .addOnSuccessListener {
                    progressDialog.dismiss()
                    Toast.makeText(applicationContext, "File Uploaded", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    progressDialog.dismiss()
                    Toast.makeText(applicationContext, "Failed", Toast.LENGTH_SHORT).show()
                }
                .addOnProgressListener {taskSnapshot ->
                    val progress = 100.0 * taskSnapshot.bytesTransferred/taskSnapshot.totalByteCount
                    progressDialog.setMessage("Uploaded" + progress.toInt() + "%..." )
                }
        }
    }

    private fun showFileChooser() {
        val intent = Intent()
        intent.type="image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "SELECT PICTURE"), PICK_IMAGE_REQUEST)
    }
}