package com.ucbcba.studentexchange.activities

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ucbcba.studentexchange.R
import com.ucbcba.studentexchange.adapters.AdapterListener
import com.ucbcba.studentexchange.adapters.AdapterPendingListener
import com.ucbcba.studentexchange.adapters.AdapterRequestListener
import com.ucbcba.studentexchange.fragments.DestinationsFragment
import com.ucbcba.studentexchange.fragments.MyRequestsFragment
import com.ucbcba.studentexchange.fragments.PendingRequestsFragment
import com.ucbcba.studentexchange.fragments.ProfileFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : AppCompatActivity(), AdapterListener, AdapterPendingListener, AdapterRequestListener  {

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.main_button_navigation_view_menu,menu);
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        setSupportActionBar(toolbar)
        supportFragmentManager.beginTransaction().replace(
            R.id.main_frame_layout,
            ProfileFragment()
        ).commit()
        main_button_navigation_view.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.my_requests_id -> {
                    supportFragmentManager.beginTransaction().replace(
                        R.id.main_frame_layout,
                        MyRequestsFragment(this)
                    ).commit()
                    true
                }

                R.id.pending_request_id -> {
                    supportFragmentManager.beginTransaction().replace(
                        R.id.main_frame_layout,
                        PendingRequestsFragment(this)
                    ).commit()
                    true
                }

                R.id.destinations_id -> {
                    supportFragmentManager.beginTransaction().replace(
                        R.id.main_frame_layout,
                        DestinationsFragment(this)
                    ).commit()
                    true
                }

                R.id.profile_id -> {
                    supportFragmentManager.beginTransaction().replace(
                        R.id.main_frame_layout,
                        ProfileFragment()
                    ).commit()
                    true
                }

                R.id.logout_id -> {
                    FirebaseAuth.getInstance().signOut()
                    finish()
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    true
                }
                else -> false
            }
        }
    }

    override fun onSendClick(destino:String) {
        val db : FirebaseFirestore = FirebaseFirestore.getInstance()
        val request: MutableMap<String, Any> = HashMap()
        request["sender"] = FirebaseAuth.getInstance().currentUser?.email.toString()
        request["receiver"] = destino
        request["status"] = "Pending"
        db.collection("requests").document()
            .set(request)
        Toast.makeText(this,"sended",Toast.LENGTH_LONG).show()
    }

    override fun onAcceptClick(destino: String) {
        val db: FirebaseFirestore = FirebaseFirestore.getInstance()
        val emailUser = FirebaseAuth.getInstance().currentUser!!.email.toString()
        val docRef =  db.collection("requests")
       docRef.whereEqualTo("receiver", emailUser).whereEqualTo("sender",destino).get()
           .addOnCompleteListener { task ->
               if (task.isSuccessful) {
                   for (document in task.result!!) {
                       if (document.exists()) {
                           db.collection("requests").document(document.id).update("status","Accepted")
                       }
                   }
               } else {
                   Toast.makeText(
                       this,
                       task.exception.toString(),
                       Toast.LENGTH_SHORT
                   ).show()
               }
           }
    }

    override fun onRejectClick(destino: String) {
        val db: FirebaseFirestore = FirebaseFirestore.getInstance()
        val emailUser = FirebaseAuth.getInstance().currentUser!!.email.toString()
        val docRef =  db.collection("requests")
        docRef.whereEqualTo("receiver", emailUser).whereEqualTo("sender",destino).get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    for (document in task.result!!) {
                        if (document.exists()) {
                            db.collection("requests").document(document.id).update("status","Reject")
                        }
                    }
                } else {
                    Toast.makeText(
                        this,
                        task.exception.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
    }

    override fun onDeleteRequest(destino: String) {
        val db: FirebaseFirestore = FirebaseFirestore.getInstance()
        val emailUser = FirebaseAuth.getInstance().currentUser!!.email.toString()
        val docRef =  db.collection("requests")
        docRef.whereEqualTo("receiver", destino).whereEqualTo("sender",emailUser).get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    for (document in task.result!!) {
                        if (document.exists()) {
                            db.collection("requests").document(document.id).delete()
                        }
                    }
                } else {
                    Toast.makeText(
                        this,
                        task.exception.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
    }

}