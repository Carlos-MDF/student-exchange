package com.ucbcba.studentexchange.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ucbcba.studentexchange.interfaces.IContractLogin
import android.view.View
import com.ucbcba.studentexchange.R
import com.ucbcba.studentexchange.presenters.LoginPresenter
import kotlinx.android.synthetic.main.login_activity.*

class LoginActivity : AppCompatActivity(), IContractLogin.View {

    private lateinit var loginPresenter: IContractLogin.Presenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)
        loginPresenter = LoginPresenter(this,applicationContext)
        login_button.setOnClickListener {
            loginPresenter.login(email_edit_text.text.toString(), password_input_layout.editText?.text.toString())
        }
        sign_up.setOnClickListener{
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
    }

    override fun showErrorEmail(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        email_input_layout.error = message
        email_edit_text.error = message
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
        groupLogin.visibility = View.GONE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
        groupLogin.visibility = View.VISIBLE
    }

    override fun loginSuccess() {
        val intent = Intent(this, WelcomeActivity::class.java)
        startActivity(intent)
    }

    override fun loginFailed(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}