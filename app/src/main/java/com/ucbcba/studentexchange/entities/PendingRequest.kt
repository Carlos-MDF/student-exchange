package com.ucbcba.studentexchange.entities

data class PendingRequest(val status: String, val destiny: String , val user: String)