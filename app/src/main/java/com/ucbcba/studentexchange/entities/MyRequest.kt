package com.ucbcba.studentexchange.entities

data class MyRequest(val status: String, val destiny: String, val user: String)