package com.ucbcba.studentexchange.entities

data class User(
    val city:String? = null,
    val country:String? = null,
    val email: String? = null,
    val firstName: String? = null,
    val lastName: String? = null,
    val university:String? = null
)