package com.ucbcba.studentexchange.entities

class Request (
    val receiver: String? = null,
    val sender: String? = null,
    val status: String? = null
)