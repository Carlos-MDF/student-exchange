package com.ucbcba.studentexchange.entities

data class Destination(val destiny: String, val destination_user: String)